# bool_ret = False
# print(bool_ret)

# bool_ret = True
# print(bool_ret)

# # <class 'bool'>
# print(type(bool_ret))


# print(1 + 1)

from operator import getitem
list_int = [2,10]
print(getitem(list_int, 0))
print(getitem(list_int, 1))
# '''
# 访问元素的两种方式
# '''

# # 多重赋值--我们必须有足够的变量来对应接受完列表的所有值。
# x, y  = list_int
# print(f"x:{x}",f"y:{y}")

# # 下标索引--从0开始计数
# x = list_int[0]
# y = list_int[1]
# print(f"x:{x}",f"y:{y}")

print(len(list_int))

# from operator import add, mul
# list = [912,16.53,True,'hello', 'java','C++','Go', 2//2,2/2,add(2,mul(2,5)),["Linux is not unix", "html5, css3 ,js"]]

# # 计数器
# list_count = 0
# # len函数计算列表长度
# list_length = len(list)

# # 迭代打印每个元素
# while list_count < list_length:
#     print(list[list_count])
#     list_count += 1 # 计数器自增

# # 列表拼接和乘法
# list = [1,2]
# list += [3,4]
# print(list)

# list *= 2
# print(list)

# list = list + [5,6]*3
# print(list)


list = [2,3,4,5]
x = 2 in list
print(x) # 输出true

x = 1 in list
print(x) # 输出false

matrix = [[1,2],[3,4]]
x = matrix[0][1]
print(x) # 输出2
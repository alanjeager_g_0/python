from abc import ABC, abstractmethod


class Map(ABC):
    @abstractmethod
    def put(self, key, value):
        pass

    @abstractmethod
    def get(self, key):
        pass

    @abstractmethod
    def remove(self, key):
        pass

    @abstractmethod
    def clear(self):
        pass

    @abstractmethod
    def size(self):
        pass

    @abstractmethod
    def contains_key(self, key):
        pass

    @abstractmethod
    def contains_value(self, key, value):
        pass

    def is_empty(self):
        return self.size() == 0




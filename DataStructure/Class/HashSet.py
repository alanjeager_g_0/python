# from Interface.Set import Set
#
# """
# 数组：哈希表的底层存储结构，用来存放键值对的桶（bucket）.
# 链表： 解决哈希碰撞。--单个桶放相同哈希值的一系列值
# 红黑树： 单个桶过长将结构转化为红黑树。
# """
# class HashSet(Set):
#     class TreeNode:
#         def __init__(self, key, value):
#             self.key = key
#             self.value = value
#             self.parent = None
#             self.left = None
#             self.right = None
#             self.color = True  # 默认为红色节点
#
#     class LinkedListNode:
#         def __init__(self, key, value):
#             self.key = key
#             self.value = value
#             self.next = None
#
#     def __init__(self,_capacity: int = 17):
#         self._capacity = _capacity if _capacity > 17 else 17
#         self.size = 0
#         self.rbt_threshold = 8   # 临界因子,单链表转化成红黑树的阈值
#         self.link_threshold = 6  # 临界因子，红黑树还原成单链表的阈值----经过remove方法，长度下降到6开始
#         self.buckets = [None] * self._capacity
#
#     def __len__(self):
#         return self.size
#
#     @staticmethod
#     def hash(self, key):
#         return hash(key)%self._capacity
#
#     def put(self, key, value):
#         index = self.hash(self, key)
#         if self.buckets[index] is None:
#             self.buckets[index] = self.LinkedListNode(key, value)
#         else:
#             cur = self.buckets[index]
#             while cur.next is not None:
#                 cur = cur.next
#             cur.next = self.LinkedListNode(key, value)
#         self.size += 1
#
#     def get(self, key):
#         index = self.hash(self, key)
#         if self.buckets[index] is None:
#             return None
#         else:
#             cur = self.buckets[index]
#             while cur is not None:
#                 if cur.key == key
#                     return cur.value
#             else:
#                 return None
#
#
#






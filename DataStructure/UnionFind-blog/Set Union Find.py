from bs4 import element


class SetUnionFind:
    def __init__(self, elements):
        self.set_list = [{element} for element in elements]

    def find_head(self, value):

        for s in self.set_list:
            if value in s:
                return s
        return None

    def union(self, head1, head2):

        set1 = self.find_head(head1)
        set2 = self.find_head(head2)

        if set1 != set2:
            set1.update(set2)
            self.set_list.remove(set2)

    def is_same_set(self, head1, head2):
        return self.find_head(head1) == self.find_head(head2)
